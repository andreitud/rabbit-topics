import json
import pika
from settings import PARAMS, EXCHANGE


def produce_to_queue(data_to_send, routing_key, exchange=''):
  # message = bytes(json.dumps(data_to_send), 'utf-8')
  message = bytes(data_to_send, 'utf-8')

  connection = pika.BlockingConnection(PARAMS)
  channel = connection.channel()
  channel.basic_publish(exchange=exchange, 
    routing_key=routing_key,
    body=message
  )
  connection.close()

def queue_consumer(binding_pair=[], message_callback=lambda msg:None):
  # Defining the callback that the library requires
  def simpler_callback(channel, method, properties, body_message):
    if body_message == b'bye':
      channel.close()
      return
    message_callback(body_message.decode())

  connection = pika.BlockingConnection(PARAMS)
  try:
    channel = connection.channel()
    queue = channel.queue_declare('', auto_delete=True)
    queue_name = queue.method.queue

    routes = []
    for exchange, routing_key in binding_pair:
      channel.queue_bind(queue_name, exchange, routing_key)
      routes.append(routing_key)

    channel.basic_consume(
      queue=queue_name,
      on_message_callback=simpler_callback,
      auto_ack=True
    )
    print(f'Consuming at\t {queue_name} on')
    for route in routes:
      print(route)
    print('-'*30)
    channel.start_consuming()
  finally:
    connection.close()
    
