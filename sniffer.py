import pika
from settings import PARAMS
from rabbit import queue_consumer


def simpler_callback(channel, method, properties, body_message):
  print(f'channel: {channel}')
  print(f'method: {method}')
  print(f'body_message: {body_message}')
  print(f'headers: {properties.headers.get("name", None)}')
  print('-'*30)

  

  if body_message == b'bye':
    channel.close()
    return

if __name__ == "__main__":
  PARAMS.virtual_host = '/'
  connection = pika.BlockingConnection(PARAMS)
  channel = connection.channel()
  queue = channel.queue_declare(queue='', auto_delete=True)
  queue_name = queue.method.queue
  # channel.queue_bind(queue_name, 'amq.rabbitmq.event', 'queue.created')
  # channel.queue_bind(queue_name, 'amq.rabbitmq.event', 'queue.deleted')
  # channel.queue_bind(queue_name, 'amq.rabbitmq.event', 'channel.closed')
  channel.queue_bind(queue_name, 'amq.rabbitmq.event', 'consumer.created')
  channel.queue_bind(queue_name, 'amq.rabbitmq.event', 'consumer.deleted')
  
  # Consuming
  try:
    print(f'Listening for events on {queue_name}')
    channel.basic_consume(queue_name, simpler_callback, True)
    channel.start_consuming()
  except KeyboardInterrupt:
    print(" Interrupted (:")
  finally:
    channel.close()
    print("Bye byeee~")