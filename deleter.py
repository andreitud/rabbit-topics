import pika
from settings import PARAMS
RABBIT_API_TOKEN = 'CGnUYScG0jOIPxDrOEQ4hdC85Yi6LMNL1g'


uuids = [
    'customer.de56ed9a-1724-4f93-bc7b-a9108647a77f',
    'customer.0658d307-64b2-4744-a5b4-6a525545c003',
    'customer.8797a8a9-f7cc-4ad9-aa66-ea4d10c2565c',
    'customer.4d691242-b77f-4bc2-bf13-548e4ecfcc47',
    'customer.1b0898cd-c14d-41f8-965d-d85ece72f4a4',
    'customer.e5bc195c-d05d-4000-b7b4-109e93e5aebb'
]

cred = pika.PlainCredentials(RABBIT_API_TOKEN, '')
PARAMS.credentials = cred

con = pika.BlockingConnection(PARAMS)
ch = con.channel()
for queue in uuids:
    ch.queue_delete(queue)
ch.close()
con.close()