include .env
export 

SVENV =. venv/bin/activate

requirements:
	$(SVENV) && pip install -r requirements.txt

m=''
cons:
	$(SVENV) && python -m take_test.consumer ${m}

m=''
prod:
	$(SVENV) && python -m take_test.producer ${m}
