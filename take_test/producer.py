import os, sys, json
import pika
from settings import EXCHANGE
from rabbit import produce_to_queue

# args: routing_key, message
if __name__ == '__main__':
  
  with open('take_test/prod.json', 'r') as f:
    data = json.load(f)

  if len(sys.argv) > 1 and sys.argv[1]:
    key = f'_{sys.argv[1]}'
    data = data[key]
  else:
    data = data[data['key']]

  token = data.get('token', '')
  routing = data.get('routing', '')
  exchange = data.get('exchange', '')
  vhost = data.get('vhost', '/')
  message = data.get('message', '-')
  
  # print(f'Message: {message}')
  # print(f'Routing: {routing}')

  message = bytes(json.dumps(message), 'utf-8')

  cred = pika.PlainCredentials(token, '')
  params = pika.ConnectionParameters(virtual_host=vhost, credentials=cred)

  connection = pika.BlockingConnection(params)
  channel = connection.channel()
  channel.basic_publish(exchange=exchange, 
    routing_key=routing,
    body=message
  )
  connection.close()