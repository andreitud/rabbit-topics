import os, sys, json
import pika
from settings import *



def message_callback(msg):
  print(msg)

if __name__ == "__main__":

  # Extracting args
  with open('take_test/cons.json', 'r') as f:
    data = json.load(f)
  if len(sys.argv) > 1:
    key = f'_{sys.argv[1]}'
    data = data[key]
  if data.get('key'):
    data = data[data['key']]
  queue = data['queue']
  bindings = data['bindings']
  auth_token = data['auth_token']
  vhost = data.get('vhost') or 'hyundai_host'

  # Simpler consumer callback
  def simpler_callback(channel, method, properties, body_message):
    if body_message == b'bye':
      channel.close()
      return
    message_callback(body_message.decode())
  
  # Create params
  params = pika.ConnectionParameters(
    virtual_host=vhost,
    host='localhost',
    credentials=pika.credentials.PlainCredentials(auth_token, '')
  )

  # Init the connection
  connection = pika.BlockingConnection(params)
  channel = connection.channel()

  print(f"Consuming on {queue}")
  for binds in bindings:
    print(f'\t{binds}')
  print('-'*60)

  # Consuming
  try:
    channel.basic_consume(queue, simpler_callback, True)
    channel.start_consuming()
  except KeyboardInterrupt:
    channel.close()
    print(" Interrupted (:")
  finally:
    print("Bye byeee~")


  