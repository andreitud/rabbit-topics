import os
import pika
from dotenv import load_dotenv
import logging

load_dotenv()

RABBIT_USER = os.environ.get('RABBIT_USER', None)
RABBIT_PASS = os.environ.get('RABBIT_PASS', None)
RABBIT_QUEUE = os.environ.get('RABBIT_QUEUE', 'queue')
RABBIT_HOST = os.environ.get('RABBIT_HOST', 'localhost')
RABBIT_VHOST = os.environ.get('RABBIT_VHOST', '/')

API_TOKEN = 'CGnUYScG0jOIPxDrOEQ4hdC85Yi6LMNL1g'

PARAMS = pika.ConnectionParameters(
  virtual_host=RABBIT_VHOST,
  host=RABBIT_HOST,
  credentials=pika.PlainCredentials(API_TOKEN, '')
)

CONSUMER_PIDFILE = os.environ.get('CONSUMER_PIDFILE', '/')
EXCHANGE = os.environ.get('EXCHANGE', '')

logger = logging.Logger('consumer_logger.log')
